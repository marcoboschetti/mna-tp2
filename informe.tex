\documentclass[a4paper]{article}

%% Sets page size and margins
\usepackage[margin=1.1in]{geometry}
\usepackage{setspace}
\spacing{1.5}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{cite}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{multicol}
\usepackage{graphicx}


\title{\textbf{Informe TP Nro.2 \\
Integer FFT and Image Compression \\}}

\date{18-11-2016}

\author{Marco Boschetti - 55266\\
Juan Cruz Lepore - 55124\\
Matías Mercado - 55019\\
Guido Matías Mogni - 55156\\
María Milagros Moreno Lafon - 55332 \\ \\}


\begin{document}
\maketitle

\pagenumbering{gobble}
\newpage
\pagenumbering{arabic}

\begin{center}
\section*{Resumen}
\end{center}

\paragraph{}

El presente trabajo tiene por objetivo aplicar la Transformada Discreta Coseno Bidimensional (TDC2) a imágenes en escala de grises, a fin de reducir el tamaño de las mismas al ser comprimidas.

Para ello se implementó una versión de la Transformada Rápida de Fourier (FFT) que transforma números enteros a números complejos con coeficientes enteros.

Asimismo, para recuperar la imagen transformada y verificar que no haya pérdida de información, se implementó la inversa de la transformada previamente mencionada.

$\\$
$\\$
PALABRAS CLAVE: transformada discreta coseno bidimensional (TDC2), Huffman, Integer FFT, image compression, butterfly, lifting matrix.

\newpage

\section{Introducción}
\paragraph{}
En términos generales, para reducir el tamaño de una imagen después de ser comprimida, se la transforma previamente utilizando la TDC2.

Debido a la precisión limitada de las operaciones con punto flotante, resulta interesante la utilización de la Integer FFT\cite{InvertibleIntegerDCT}, función que aplica la Transformada Rápida de Fourier a enteros y retorna números complejos con coeficientes enteros. La principal ventaja de esta función es que, si bien los cálculos intermedios se realizan con precisión de punto flotante, la matriz resultante queda representada por enteros y por lo tanto la función resulta invertible, lo cual posibilita una transformación sin pérdida de información.

En primer lugar se realiza un análisis detallado de la implementación de la Integer FFT, para la cual se utilizaron Matrices de Lifting y Butterflies.

Luego se presenta la implementación de la TDC2 limitada a imágenes en escala de grises (donde cada pixel posee hasta 255 valores diferentes) y cuyas dimensiones sean múltiplos de 8x8 px.

Como adicional, se realiza una implementación de la TDC2 para imágenes a color, de dimensiones múltiplos de 8x8 px.

Finalmente, se utiliza el algoritmo de Huffman para analizar la eficiencia de la TDC2 comparando el tamaño de las imágenes originales comprimidas con las imágenes transformadas comprimidas.

\newpage

\section{Metodología}
\paragraph{}
Para la implementación de los problemas planteados se utilizó Octave y los códigos fuente del mismo se encuentran en el \href{https://bitbucket.org/marcoboschetti/mna-tp2}{repositorio de BitBucket} \cite{repository}.

\subsection{Manejo de imágenes}
A fin de determinar la mejora en la compresión de la imagen al transformarla con la TDC2, se utilizaron 10 imágenes en escala de grises presentes en la sección \ref{sec:anexo}Anexo I.

Dentro de la función main, se utiliza el método \emph{imread} para cargar la imagen que se desea comprimir en una matriz, donde cada celda representa un píxel con un valor de gris entre 0 y 255. Esta matriz es separada en submatrices de 8x8, transformada utilizando la transformada del coseno bidimensional (DCT2) y unida nuevamente para obtener una matriz de las mismas dimensiones que la original.

Por último, se analiza el tamaño de las matriz original y transformada comprimidas con el método de Huffman, para determinar la eficacia del método.

$\\$
\subsection{Implementación de FFT}
\subsubsection{Matrices de Lifting}
\paragraph{}
Como se indica en el paper anteriormente mencionado, la función a implementar debe tomar valores enteros, aplicando una transformada rápida de Fourier, y retornar valores complejos con coeficientes enteros. 
Tomando como ejemplo el siguiente producto de un vector por una matriz de \emph{lifting} triangular superior:

\[
\begin{bmatrix}
   y_{1} \\
   y_{2}
\end{bmatrix}
 =
\begin{bmatrix}
    1 & \alpha \\
    0 & 1
\end{bmatrix}
\begin{bmatrix}
   x_{1} \\
   x_{2}
\end{bmatrix}
\]

$\\$
se define la siguiente operación reversible:
\begin{align*}
y_{1} =& x_{1} + \lfloor \alpha x_{2} \rfloor\\
y_{2} =& x_{2}
\end{align*}

donde, si el vector original es de valores enteros, entonces $y_{1}$ e $y_{2}$ también son enteros. Para recuperar los valores originales, basta con aplicar la siguiente operación inversa:

\begin{align*}
x_{2} =& y_{2}\\  
x_{1} =& y_{1} - \lfloor 
\alpha x_{2} \rfloor
\end{align*}

Estas operaciones permiten que el resultado de la transformada rápida de Fourier sean valores complejos de coeficientes enteros, logrando el objetivo planteado en la introducción del trabajo.

Se destacan dos tipos de matrices de tipo Lifting:

1) Scaling:
\[
\begin{bmatrix}
    k & 0 \\
    0 & \frac{1}{k}
\end{bmatrix}
=
\begin{bmatrix}
    1 & k-k^2 \\
    0 & 1
\end{bmatrix}
\begin{bmatrix}
    1 & 0 \\
    -1/k & 1
\end{bmatrix}
\begin{bmatrix}
    1 & k-1 \\
    0 & 1
\end{bmatrix}
\begin{bmatrix}
    1 & 0 \\
    1 & 1
\end{bmatrix}
\]

2)Rotation:
\[
\begin{bmatrix}
    cos\alpha & -sin\alpha \\
    sin\alpha & cos\alpha
\end{bmatrix}
=
\begin{bmatrix}
    1 & -tg(\frac{\alpha}{2}) \\
    0 & 1
\end{bmatrix}
\begin{bmatrix}
    1 & 0 \\
    sin\alpha & 1
\end{bmatrix}
\begin{bmatrix}
    1 & -tg(\frac{\alpha}{2}) \\
    0 & 1
\end{bmatrix}
\]

$\\$
\subsubsection{Butterflies}
\paragraph{}
Se recibe un vector de 8 elementos, los cuales son todos enteros no complejos y se procede a realizar la Integer Fast Fourier Transform (FFT). Se dividió dicha transformación en 4 partes distintas. Cada parte de este proceso posee una matriz butterfly distinta, el orden de ejecución de cada parte se muestra en la siguiente figura:


\begin{figure}[h]
\includegraphics[scale=0.5]{esquema.png}
\centering
\caption{Esquema donde se muestra el orden de multiplicación de cada elemento del vector por la matriz butterfly correspondiente a la etapa de la FFT en la que se encuentra. Donde cada cruz(X) simboliza un entero no complejo y los círculos(O) representan a los enteros complejos.}
\label{fig:butterflies}
\end{figure}

\paragraph{}
Donde cada cruz simboliza un entero no complejo y los círculos representan a los enteros complejos.

\subsection{Extra: Imágenes en RGB}
Como extra, se decidió implementar la compresión que se realiza actualmente a las imágenes en escala de grises, en imágenes con otros colores. El comportamiento del algoritmo es básicamente el mismo, debido a que si se divide la imagen en tres de igual tamaño, conteniendo cada una para cada píxel los valores correspondientes a cada uno de los colores primarios de la luz, RGB(red, green, blue), se puede aplicar la transformada a cada matriz individualmente, y luego pasando a al algoritmo de \emph{Huffman} un vector con las ya mencionadas matrices transformadas.
Para cargar la imagen se utiliza nuevamente el método imread, este método adapta su salida al tipo de imagen de entrada, devolviendo para el caso de imágenes a color, una matriz de tres dimensiones, donde el tercer elemento de la tupla indica el color que está siendo representado por la matriz bidimensional de los correspondientes dos primeros elementos de la tupla, siendo 1 = R, 2 = G, y 3 = B.
En caso de querer recuperar la imagen original, basta con aplicar la inversa individualmente a cada una de las matrices de color, y asignar las nuevas a un vector de matrices, esto es interpretable por Octave como una imagen y puede ser persistido de la forma que se desee.

\paragraph{}

La primera etapa consiste en multiplicar por la suigiente matriz para cada par de enteros en el orden que se muestra en la Figura 1, a esta matriz se la llamó ButterflyA.

\[
\begin{bmatrix}
   b_{1} \\
   b_{2}
\end{bmatrix}
 =
\begin{bmatrix}
    \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} \\
    \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}}
\end{bmatrix}
\begin{bmatrix}
   a_{1} \\
   a_{2}
\end{bmatrix}
\]

Donde  $b_{1}$ y $b_{2}$ son enteros no complejos.
Para que esto sea posible se dividió a la matriz butterfly en un producto de matrices de tipo Lifting:
\[
\begin{bmatrix}
    \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} \\
    \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}}
\end{bmatrix}
=
\begin{bmatrix}
    1 & 0 \\
    1 & 1
\end{bmatrix}
\begin{bmatrix}
    1 & -\frac{1}{2} \\
    0 & 1
\end{bmatrix}
\begin{bmatrix}
    \frac{2}{\sqrt{2}} & 0 \\
    0 & \sqrt{2}
\end{bmatrix}
\begin{bmatrix}
    1 & 0 \\
    0 & -1
\end{bmatrix}
\]


Donde $\begin{bmatrix} 1 & 0 \\ 1 & 1 \end{bmatrix} y \begin{bmatrix} 1 & -\frac{1}{2} \\ 0 & 1\end{bmatrix}$  son matrices de Lifting y $\begin{bmatrix} \frac{2}{\sqrt{2}} & 0 \\ 0 & \sqrt{2} \end{bmatrix}$ es una matriz de Scaling, permitiendo poder hacer el producto y asegurando que de un resultado entero.
Por último, la matriz $\begin{bmatrix} 1 & 0 \\0 & -1 \end{bmatrix}$ no es una matriz de tipo Lifting, pero dado que tiene todos sus coeficientes enteros, cualquier vector entero multiplicado por esta matriz da como resultado un vector entero. 

Para la segunda etapa se realiza la siguiente operación con una nueva butterfly a la que se llamó ButterflyB.

\[
\begin{bmatrix}
   b_{1} \\
   b_{2}
\end{bmatrix}
 =
\begin{bmatrix}
    1 & i \\
    1 & -i
\end{bmatrix}
\begin{bmatrix}
   a_{1} \\
   a_{2}
\end{bmatrix}
\]


Donde $a_{1}$ y $a_{2}$ son enteros no complejos y $b_{1}$  y $b_{2}$ son enteros complejos; por lo tanto, el producto con la ButterflyB se hace con un vector de enteros no complejos y se obtiene como resultado un vector de enteros complejos.
Dados los coeficientes de ButterflyB, al realizar un producto con cualquier vector entero, el resultado siempre será un vector de enteros complejos.
Cabe destacar que en esta etapa se usó tanto la ButterflyA como la ButterflyB, dependiendo del resultado que indica el esquema de la Figura 1.

En la tercera etapa se definió una tercer matriz butterfly a la que se llamó ButterflyC 

\[
\begin{bmatrix}
   b_{1} \\
   b_{2}
\end{bmatrix}
 =
\begin{bmatrix}
    \frac{1}{\sqrt{2}} & \frac{W^j_{N}}{\sqrt{2}} \\
    \frac{1}{\sqrt{2}} & -\frac{W^j_{N}}{\sqrt{2}}
\end{bmatrix}
\begin{bmatrix}
   a_{1} \\
   a_{2}
\end{bmatrix}
\]  

Donde $W^j_{N}$  = $e^\frac{-ij2\pi}{N}$ y $a_{1}$, $a_{2}$, $b_{1}$ y $b_{2}$ son enteros complejos.

\paragraph{}
Luego se procedió a descomponer la ButterflyC en matrices de tipo Lifting.


\[
\begin{bmatrix}
    \frac{1}{\sqrt{2}} & \frac{W^j_{N}}{\sqrt{2}} \\
    \frac{1}{\sqrt{2}} & -\frac{W^j_{N}}{\sqrt{2}}
\end{bmatrix}
=
\begin{bmatrix}
    1 & 0 \\
    1 & 1
\end{bmatrix}
\begin{bmatrix}
    1 & -\frac{1}{2} \\
    0 & 1
\end{bmatrix}
\begin{bmatrix}
    \frac{2}{\sqrt{2}} & 0 \\
    0 & \sqrt{2}
\end{bmatrix}
\begin{bmatrix}
    1 & 0 \\
    0 & -W^j_{N}
\end{bmatrix}
\]

Donde $\begin{bmatrix} 1 & 0 \\ 1 & 1 \end{bmatrix}$ y $\begin{bmatrix} 1 & -\frac{1}{2} \\ 0 & 1\end{bmatrix}$  son matrices de Lifting y $\begin{bmatrix} \frac{2}{\sqrt{2}} & 0 \\ 0 & \sqrt{2} \end{bmatrix} $es una matriz de Scaling, asegurando que el producto siempre resulta en valores enteros.

Luego se analizó el producto de un vector por la última matríz de la descomposición de la ButterflyC.


\[
\begin{bmatrix}
   a_{1} \\
   -W^j_{N} a_{2}
\end{bmatrix}
 =
\begin{bmatrix}
    1 & 0 \\
    0 & -W^j_{N}
\end{bmatrix}
\begin{bmatrix}
   a_{1} \\
   a_{2}
\end{bmatrix}
\]

Se procedió a analizar el producto $W^j_{N} a_{2}$. Teniendo en cuenta que $a_{2}$ es un entero complejo, se lo pudo expresar como $a_{2} = x + iy$ y se expresó $W^j_{N} =  e^\frac{-ij2\pi}{N} = cos(\frac{j2\pi}{N}) - isen(\frac{j2\pi}{N})$. Con las nuevas expresiones se consiguó escribir el siguiente producto matricial:


\[
\begin{bmatrix}
   \beta_{1} \\
   \beta_{2}
\end{bmatrix}
 =
\begin{bmatrix}
    cos\alpha & -sin\alpha \\
    sin\alpha & cos\alpha
\end{bmatrix}
\begin{bmatrix}
   x \\
   y
\end{bmatrix}
\]

Donde $\alpha = -\frac{j2\pi}{N}$.
\paragraph{}
Se vió que la matriz $\begin{bmatrix} cos\alpha & -sin\alpha \\ sin\alpha & cos\alpha \end{bmatrix} $corresponde con una matriz de tipo Rotation que posee una descomposición en matrices de Lifting y, por lo tanto, garantiza que el resultado del producto es un número entero complejo.
Se tuvo entonces el siguiente resultado:

\[
\begin{bmatrix}
   a_{1} \\
   -\beta_{1}-i\beta_{2}
\end{bmatrix}
 =
\begin{bmatrix}
    1 & 0 \\
    0 & -W^j_{N}
\end{bmatrix}
\begin{bmatrix}
   a_{1} \\
   a_{2}
\end{bmatrix}
\]

Donde $a_{1}$ es un entero complejo y por lo explicado anteriormente $-\beta_{1}-i\beta_{2}$ también es un entero complejo.

Por último se destaca que en esta tercer etapa se aplican las 3 matrices butterfly mencionadas anteriormente, dependiento del diagrama de la Figura 1. Si se recibe un par de enteros no complejos se utiliza la ButterflyA, devolviendo un par de elementos no complejos, si se recibe un par de enteros no complejos se utiliza la ButterflyB y se devuelve un par de elementos complejos y para el caso en el que se reciben enteros complejos se utiliza la ButterflyC y se devuelven enteros complejos.

En la última etapa de la FFT se le asignó al cuarto y octavo elemento del vector resultante de la etapa anterior, el conjugado del sexto y segundo elemento respectivamente, finalizando de esta manera el proceso de la FFT.
Cabe destacar todos los elementos del vector resultante de este proceso son enteros complejos, excepto el primer y quinto elemento, los cuales son enteros no complejos.

\subsection{Transformada de Coseno Discreta}
Con el objetivo de optimizar la compresión de imágenes, como se planteó previamente, fue necesario implementar la transformada de coseno discreta (TDC). Para esto se implementaron dos funciones: la TDC unidimensional (que opera sobre vectores de 8 elementos) y la TDC bidimensional (que recibe como argumento matrices de 8x8).

Dentro de la implementación de la transformada de coseno discreta unidimensional, se genera un vector de 8 elementos cuyas componentes son las del vector recibido como parámetro reordenadas, con el fin de aplicarle la transformada discreta de Fourier de enteros descrita previamente. Una vez que se aplica la FFT, se obtiene un nuevo vector, a partir del cual se realiza una rotación por medio de la aplicación sucesiva de 3 matrices de lifting, para luego obtener un nuevo vector de 8 elementos enteros.

Dado que la transformada de coseno discreta unidimensional opera sobre arreglos de 8 elementos y el objetivo del proyecto es aplicar la transformación en submatrices de la imagen original de 8x8, fue necesario implementar una segunda función capaz de operar sobre estas submatrices. La transformada de coseno bidimensional, en primer lugar, itera sobre las filas de la submatriz que recibe como parámetro, obteniendo la TDC unidimensional de cada una. Luego de este primer ciclo, se obtiene una nueva matriz donde cada fila resulta de aplicar TDC unidimensional a la matriz original. Esta nueva matriz se itera por columnas para aplicar la TDC unidimensional nuevamente. Una vez ejecutados estos dos ciclos, la función TDC bidimensional retorna la submatriz transformada.

Dado que tanto la FFT definida en el presente informe como la aplicación de operaciones de lifting son invertibles, como ya se mostró previamente, la operación definida como TDC unidimensional también lo es. Por lo tanto, la transformada de coseno discreta bidimensional resulta ser invertible, requisito para poder realizar la compresión de una imagen sin pérdida de información.

\section{Extra: Imágenes en RGB}
Como extra, se decidió implementar la compresión que se realiza actualmente a las imágenes en escala de grises, en imágenes con otros colores. El comportamiento del algoritmo es básicamente el mismo, debido a que si se divide la imagen en tres de igual tamaño, conteniendo cada una para cada píxel los valores correspondientes a cada uno de los colores primarios de la luz, RGB(red, green, blue), se puede aplicar la transformada a cada matriz individualmente, y luego pasando a al algoritmo de \emph{Huffman} un vector con las ya mencionadas matrices transformadas.

Para cargar la imagen se utiliza nuevamente el método imread, este método adapta su salida al tipo de imagen de entrada, devolviendo para el caso de imágenes a color, una matriz de tres dimensiones, donde el tercer elemento de la tupla indica el color que está siendo representado por la matriz bidimensional de los correspondientes dos primeros elementos de la tupla, siendo 1 = R, 2 = G, y 3 = B.

En caso de querer recuperar la imagen original, basta con aplicar la inversa individualmente a cada una de las matrices de color, y asignar las nuevas a un vector de matrices, esto es interpretable por Octave como una imagen y puede ser persistido de la forma que se desee.

A continuación se muestra el resultado de aplicar este algoritmo a una imagen a color, seguido de \emph{Huffman} y comparado con el resultado de aplicar \emph{Huffman} sin pasar por el algoritmo de transformación.

\begin{table}[]
\centering
\caption{My caption}
\label{my-label}
\begin{tabular}{lllll}
Archivo    & Original & Huffman & TDC + Huffman & Tasa de optimización \\
flan.bmp  & 6291456  & 5980080 & 4066211       & 32\%                 \\
\end{tabular}
\end{table}


\newpage
\section{Resultados y Conclusiones}
A continuación se presenta la comparación de los tamaños de las imágenes comprimidas mediante el método de Huffman antes y después de implementar la transformada del coseno bidimensional sobre la matriz de píxeles.

\begin{table}[]
\centering
\caption{My caption}
\label{my-label}
\begin{tabular}{lllll}
Archivo    & Original & Huffman & TDC + Huffman & Tasa de optimización \\
panda.jpg  & 2097152  & 2021411 & 1450338       & 28\%                 \\
kraken.jpg & 6291456  & 4396227 & 1357680       & 69\%                 \\
tree.jpg   & 6291456  & 5598681 & 1767358       & 68\%                 \\
ghosts.jpg & 5529600  & 3304529 & 1226500       & 63\%                 \\
kirby.png  & 1572864  & 249132  & 112540        & 55\%                 \\
linux.png  & 6291456  & 1233960 & 926426        & 25\%                 \\
fox.jpg    & 2097152  & 2055879 & 1365513       & 34\%                 \\
clown.bmp  & 512000   & 430563  & 410930        & 5\%                  \\
lena.jpg   & 2097152  & 2003337 & 1413110       & 29\%                 \\
god.jpg    & 2097152  & 2064385 & 1552278       & 25\%                
\end{tabular}
\end{table}

Puede observarse cómo en los casos seleccionados se obtiene una mejora promedio de 40.2\% aplicando la función transformada del coseno discreta bidimensional antes de aplicar la codificación con el método de Huffman.
Dentro del muestreo de 10 imágenes analizadas, se puede apreciar que aquellas imágenes con menos variación de color resultan en una tasa de compresión aún mayor, en comparación con aquellas imágenes que presentan más cantidad de valores únicos en escala de grises.

Cabe destacar que en el paper mencionado previamente \cite{InvertibleIntegerDCT}, en la butterflyC, cuando define el producto de $W^j_{N}$x$a_{2}$ como un producto matricial de la parte real y la parte imaginaria de $a_{2}$ por la matriz $\begin{bmatrix} cos\alpha & -sin\alpha \\ sin\alpha & cos\alpha \end{bmatrix} $, llama a $\alpha = -\frac{ij2\pi}{N}$. Esto es un error ya que  $\alpha = -\frac{j2\pi}{N}$ , sin $i$.

%Anexo
\newpage
\section{Anexo} \label{sec:anexo}

\newpage

\renewcommand{\refname}{Bibliografía}

\begin{thebibliography}{2}

\bibitem{InvertibleIntegerDCT} Yan Yusong, Wang Chunmei, Su Guangda, Shi Qingyun \emph{Invertible Integer DCT Applied on Progressive until Lossless Image Compression}, ISPA, 2003

\bibitem{repository}
  https://bitbucket.org/marcoboschetti/mna-tp2

\end{thebibliography}

\end{document}