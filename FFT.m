% getLifting: Hace el lifting de un vector por una matriz de lifting
% Si la matriz tiene todos coeficientes enteros, getLifting(X, M)
% es igual a M * X

% ILiftWN: Hace el lifting de un vector por una matriz del tipo 
% [1,0; 0, -WN], con WN = e^(i*2*pi*j/8) 
% (Notar que esta matriz NO es de lifting, internamete se descompone)

% IGetLifting: Hace la operacion inversa a lifting, para matrices de lifting
% Si la matriz inversa tiene todos coeficientes enteros, IGetLifting(X, M)
% es igual a inverse(M) * X

% ILiftWN: Consigue inversa para matrices del tipo: [1,0; 0, -WN], con WN = e^(i*2*pi*j/8)

function Y = FFT(M)

Mm = [M(1),M(5),M(3),M(7),M(2),M(6),M(4),M(8)];


%
% Phase A
%
for k = 1:2:8
  t = butterflyA([Mm(k);Mm(k+1)]);
 Mm(k) = t(1);
 Mm(k+1) = t(2);
endfor

%
% Phase B 
%
  t = butterflyA([Mm(1);Mm(3)]);
 Mm(1) = t(1);
 Mm(3) = t(2);
   t = butterflyA([Mm(5);Mm(7)]);
 Mm(5) = t(1);
 Mm(7) = t(2);
 
  t = butterflyB([Mm(2);Mm(4)]);
 Mm(2) = t(1);
 Mm(4) = t(2);
   t = butterflyB([Mm(6);Mm(8)]);
 Mm(6) = t(1);
 Mm(8) = t(2);
 
 
 
%
% Butterfly C 
%

t = butterflyA([Mm(1);Mm(5)]);
Mm(1) = t(1);
Mm(5) = t(2);

t = butterflyC([Mm(2);Mm(6)]);
Mm(2) = t(1);
Mm(6) = t(2);

t = butterflyB([Mm(3);Mm(7)]);
Mm(3) = t(1);
Mm(7) = t(2);
 
%Butterfly D

Mm(4) = conj(Mm(6));
Mm(8) = conj(Mm(2));
Y = Mm;

endfunction
