function transformed = untransformMatrix (m)

transformed = zeros(rows(m),columns(m));

for i = 1:8:rows(m)
	for j = 1:8:columns(m)
	
      ans = m([i:i+7], [j:j+7]);
    % Llamada a transformada....
     IDCT2(ans);   
     transformed([i:i+7], [j:j+7]) = ans;
     
  endfor
endfor

endfunction