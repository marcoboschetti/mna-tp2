function Y = DCT1(X)

X2N = [X,fliplr(X)];
XPAR = X2N(1:2:16);

XFFT = FFT(XPAR);

%XFFT = FFT(X);

for index = [2,3,4]  
  U = getU([real(XFFT(index)),imag(XFFT(index))],pi * (index-1)/16);
  Y(index) = U(1) ;
  Y(10-index) = U(2);
endfor

Y(1) = XFFT(1);
Y(5) = XFFT(5);

endfunction  



