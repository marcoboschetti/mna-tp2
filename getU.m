function Y = getU(X, alpha)
  x1 = [1, -tan(alpha/2); 0, 1];
  x2 = [1, 0; sin(alpha), 1];
  
  Y = getLifting(X, x1);
  Y = getLifting(Y, x2);
  Y = getLifting(Y, x1);
endfunction