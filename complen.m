%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%% % %Argumento de entrada
% *x= matriz a comprimir
%% % %Argumento de salida
% *lencomp= estimaci´on de la longitud luego de la compresi´on
function lencomp = complen(x)
[m,n] = size(x);
x = reshape(x,m*n,1);
x = x-min(x);
symbols = unique(x);
freq = hist(x,symbols);
p = freq/sum(freq);
dict = huffmandict(symbols,p);
lencomp = 0;
%le tengo que sumar el diccionario
lencomp = lencomp+nextpow2(max(x))*length(symbols);
for k = 1:length(symbols)
lencomp = lencomp + length(dict{k});
end
%le tengo que sumar la codificaci´on del tama˜no original de x
%-> + 2 entero de 32 bits
lencomp = lencomp + 2*32;
%le tengo que sumar la codificaci´on del m´ınimo
%-> asumo que con un entero de 32 bits basta
lencomp = lencomp + 32;
%le tengo que sumar el archivo comprimido
for k = 1:length(symbols)
lencomp = lencomp + freq(k)*length(dict{k});
end