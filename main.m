%Returns an array "dim" with [original, huffman, transformedHuffman]

function ans = main (fileName)

image = imread (fileName);
m = double(image);

dim1 = numel(m);
dim2 = complen(m);

transformed = transformMatrix(m);
dim3 = complen(transformed);

ans = [8*dim1,dim2,dim3];

endfunction