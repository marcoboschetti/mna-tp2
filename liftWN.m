% X = [a1, a2]
% J is the index
% return [b1, b2]

function  ret = liftWN(X, J)
  ret = zeros(2,1);
  aux = zeros(2,1);
  
  THETA = - (2 * pi * J)/ 8;  % Sacamos el "i", lo multiplicamos de nuevo al final
  
  binA = [real(X(2));imag(X(2))];
  
  M1 = [1, - tan(THETA/2); 0 , 1];
  M2 = [1, 0; sin(THETA) , 1];
  
  aux = getLifting(binA, M1);
  aux = getLifting(aux, M2);
  aux = getLifting(aux, M1);
  ret(1) = X(1);
  ret(2) = - (aux(1) + i * aux(2));
  
endfunction