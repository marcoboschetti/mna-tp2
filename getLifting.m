function ret = getLifting(X, M)
  ret = zeros(2,1);
  
  if M(1,2) == 0
    ret(1) = X(1);
    ret(2) = floor(M(2,1) * X(1)) + X(2);
  else
    ret(1) = floor(M(1,2) * X(2)) + X(1);
    ret(2) = X(2);
  endif
  
endfunction