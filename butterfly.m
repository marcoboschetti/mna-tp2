function Y = butterfly (X, j)
  Y = zeros(2, 1);
  M1 = [1,0;1,1];
  M2 = [1,-1/2;0,1];
  M3 = [1, (1/sqrt(2)) - (1/2) ; 0, 1];
  M4 = [1,0;-sqrt(2),1];
  M5 = [1, (1/sqrt(2))-1;0,1];

  Y = liftWN(X, j);
  Y = getLifting(Y, M1);
  Y = getLifting(Y, M5);
  Y = getLifting(Y, M4);
  Y = getLifting(Y, M3);
  Y = getLifting(Y, M2);
  Y = getLifting(Y, M1);
endfunction
