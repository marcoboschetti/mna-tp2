% Y = [a1, a2]
% J is the indeY
% Xurn [b1, b2]

function  X = ILiftWN(Y, J)
  X = zeros(2,1);
  aux = zeros(2,1);
  
  THETA = - (2 * pi * J)/ 8;
  
  % Multiply -1 first (In case it's not the same than doing it after transform)
  binA = -1 * [real(Y(2));imag(Y(2))];
  
  M1 = [1, - tan(THETA/2); 0 , 1];
  M2 = [1, 0; sin(THETA) , 1];
  
  aux = IGetLifting(binA, M1);
  aux = IGetLifting(aux, M2);
  aux = IGetLifting(aux, M1);
  
  X(1) = Y(1);
  X(2) = (aux(1) + i * aux(2));
  
  
endfunction