% Recivo matrix de 8x8 y doy su transformada bidimensional

function Y = IDCT2 (X)


%each column
T2 = zeros(8,8);

for i = 1:8
  T2(:,i) = IDCT1(reshape(X(:,i), 1, 8));
endfor


% each row
T1 = zeros(8,8);

for i = 1:8
  T1(i,:) = IDCT1(T2(i,:));
endfor


Y = T1;

endfunction