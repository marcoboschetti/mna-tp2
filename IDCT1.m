function X = IDCT1(Y)



for index = [2,3,4]
  U = IgetU([Y(index),Y(10-index)],pi * (index-1)/16);
  X(index) = U(1)+i*U(2) ;
  X(10-index) = U(1) - i * U(2);
endfor

X(1) = Y(1);
X(5) = Y(5);

X = IFFT(X);

X = [X(1),X(8),X(2),X(7),X(3),X(6),X(4),X(5)];

endfunction