function X = IgetU(Y, alpha)
  x1 = [1, -tan(alpha/2); 0, 1];
  x2 = [1, 0; sin(alpha), 1];
  
  X = IGetLifting(Y, x1);
  X = IGetLifting(X, x2);
  X = IGetLifting(X, x1);
endfunction