% Recivo matrix de 8x8 y doy su transformada bidimensional

function Y = DCT2 (X)

% each row
T1 = zeros(8,8);

for i = 1:8
  T1(i,:) = DCT1(X(i,:));
endfor

%each column
T2 = zeros(8,8);

for i = 1:8
  T2(:,i) = DCT1(reshape(T1(:,i), 1, 8));
endfor

Y = T2;

endfunction