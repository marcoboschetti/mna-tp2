% Y = [a1, a2]
% J is the indeY
% Xurn [b1, b2]

function X = IGetLifting(Y, M)
  X = zeros(2,1);
  
  if M(1,2) == 0
    X(1) = Y(1);
    X(2) = Y(2) - floor(M(2,1) * X(1));
  else
    X(2) = Y(2);
    X(1) = Y(1) - floor(M(1,2) * X(2));
  endif
  
endfunction