% getLifting: Hace el lifting de un vector por una matriz de lifting
% ILiftWN: Hace el lifting de un vector por una matriz del tipo 
% [1,0; 0, -WN], con WN = e^(i*2*pi*j/8) 
% (Notar que esta matriz NO es de lifting, internamete se descompone)

% Si la matriz tiene todos coeficientes enteros, getLifting(X, M)
% es igual a M * X

% IGetLifting: Consigue inversa para matrices de lifting
% ILiftWN: Consigue inversa para matrices del tipo: [1,0; 0, -WN], con WN = e^(i*2*pi*j/8)

% Si la matriz inversa tiene todos coeficientes enteros, IGetLifting(X, M)
% es igual a inverse(M) * X

function X = IbutterflyB (Y)
  X = zeros(2, 1);
  
  M1 = [1,0;1,1];
  M2 = [1,-1/2;0,1];
  M3 = [1,0; 0,2];
  M4 = [1,0;0,-i];
 
  X = IGetLifting(Y, M1);
  X = IGetLifting(X, M2);
  
  X = inverse(M3) * X; 
  X = inverse(M4) * X;
endfunction
