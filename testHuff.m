function dif = testHuff(i)

m = zeros(i,i);
m(1,1) = 1;

dif = complen(m) - numel(m);

endfunction