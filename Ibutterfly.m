function X = Ibutterfly (Y, j)
  X = zeros(2, 1);
  M1 = [1,0;1,1];
  M2 = [1,-1/2;0,1];
  M3 = [1,1/sqrt(2) - 1/2 ; 0, 1];
  M4 = [1,0;-sqrt(2),1];
  M5 = [1,1/sqrt(2)-1;0,1];
  
  X = IGetLifting(Y, M1);
  X = IGetLifting(X, M2);
  X = IGetLifting(X, M3);
  X = IGetLifting(X, M4);
  X = IGetLifting(X, M5);
  X = IGetLifting(X, M1);
  X = ILiftWN(X, j);
endfunction
