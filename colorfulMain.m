%Returns an array "dim" with [original, huffman, transformedHuffman]

function ans = colorfulMain (fileName)

image = imread (fileName);
m = double(image);

dim1 = numel(m);
dim2 = complen(m);

m(:,:,1) = transformMatrix(m(:,:,1));
"transformada 1"
m(:,:,2) = transformMatrix(m(:,:,2));
"transformada 2"
m(:,:,3) = transformMatrix(m(:,:,3));
"transformada 3"


dim3 = complen(m);

m(:,:,1) = untransformMatrix(m(:,:,1));
"antitransformada 1"
m(:,:,2) = untransformMatrix(m(:,:,2));
"antitransformada 2"
m(:,:,3) = untransformMatrix(m(:,:,3));
"antitransformada 3"

imshow(uint8(m))

ans = [dim1,dim2,dim3];

endfunction