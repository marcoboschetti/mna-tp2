function Y = IFFT(X)

Mm = X;

%
% Butterfly C 
%
t = IbutterflyA([Mm(1);Mm(5)]);
Mm(1) = t(1);
Mm(5) = t(2);

t = IbutterflyC([Mm(2);Mm(6)]);
Mm(2) = t(1);
Mm(6) = t(2);

t = IbutterflyB([Mm(3);Mm(7)]);
Mm(3) = t(1);
Mm(7) = t(2);
 
 %Butterfly D
Mm(4) = conj(Mm(2));
Mm(8) = conj(Mm(6));


%
% Phase B 
%
  t = IbutterflyA([Mm(1);Mm(3)]);
 Mm(1) = t(1);
 Mm(3) = t(2);
   t = IbutterflyA([Mm(5);Mm(7)]);
 Mm(5) = t(1);
 Mm(7) = t(2);
 
  t = IbutterflyB([Mm(2);Mm(4)]);
 Mm(2) = t(1);
 Mm(4) = t(2);
   t = IbutterflyB([Mm(6);Mm(8)]);
 Mm(6) = t(1);
 Mm(8) = t(2);
 
 
%
% Phase A
%
for k = 1:2:8
  t = IbutterflyA([Mm(k);Mm(k+1)]);
 Mm(k) = t(1);
 Mm(k+1) = t(2);
endfor

 
Y = [Mm(1),Mm(5),Mm(3),Mm(7),Mm(2),Mm(6),Mm(4),Mm(8)];


endfunction
